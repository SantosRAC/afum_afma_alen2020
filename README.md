# afum_afma_alen2020

This repository hosts most scripts used for processing raw data for the paper on antifungal susceptibility, virulence, and genomics of Spanish clinical isolates the three fungal pathogens:
* *Aspergillus fumigatus*
* *A. fumigatiaffinis*
* *A. lentulus*

We make available raw data used in drug susceptibility and virulence experiments, as well as codes used to process data (R, python 3, and Perl scripts).
Information related to sequencing data is associated to NCBI Bioproject [PRJNA592352](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA592352)

Contact information:
* **Prof. Antonis Rokas**: antonis.rokas{at}vanderbilt.edu
* **Prof. Gustavo H. Goldman**: ggoldman{at}usp.br
* **Renato A. Corrêa dos Santos**: renatoacsantos{at}gmail.com

## Data and scripts available/ covered in this repository

### Phenotypic heterogeneity

* **Susceptibility to antifungals**:
  * Raw data (experimentalData/`drugSusceptibilityData.txt`).
  * Script that generates Principal Component Analysis (PCA).
  * Script to plot drug susceptibility.
* **Virulence experiments**:
  * Raw data (experimentalData/`Galleria_Santos_et_al_2020.xlsx`).
  * Script to plot survival curves.

### Genomic analyses

* **Orthology/ OrthoFinder Analysis**:
  * Datasets: proteins of (i) the Spanish clinical strains; (ii) Spanish clinical strains + Af293.
  * Scripts to adapt the pre-computed All-vs-All blastp output for OrthoFinder input (`-og`option).
  * Script that generates the UpSet Plots.
* **SNPs and Indels**:
  * Script that recover SNP information for gene lists (SNP and INDEL effects).
recovery of genes with high impact variants used in enrichment analysis (GOATOOLS: `find_enrichment.py`).
* **Phylogenomics**:
  * [Custom Python script](https://raw.githubusercontent.com/JLSteenwyk/Phylogenetic_scripts/master/threadNuclProt.py)
  * Phylogenetic tree (phylogenomics/`busco3954.tre`)
* **Gene Ontology Enrichment Analysis**:
  * Script for the construction of GOATOOLS association file from InterProScan 5 functional annotation (.tsv).
  * Script to recover functional annotation for genes with high impact variants.
* **Polymorphisms in genes related antifungal susceptibility**:
  * Script that extracts promoter regions from genes.
  * Script for recovery of changed positions and reporting changed base/aa per site from alignment (e.g. MAFFT), for selected genes.
* **Phylogenetic analysis with the protein sequences in the Cyp51 family**:
  * Phylogeny from IQ-TREE (antifungalRelatedGenes/phylogenyCyp51GeneFamily/`Cyp51_Familty.treefile`)
* **Determinants of virulence**:
  * Annotation of the 215 virulence determinants as recovered from FungiDB (`virulenceDeterminants/virulence-related_fungiDB.txt`).
  * Script that joins functional annotation (FungiDB) and orthogroup info (OrthoFinder results).
  * Script that generates a heatmap from paralog counts for each and every determinant of virulence.
More details on orthology calling are described on our [wiki page](https://gitlab.com/SantosRAC/afum_afma_alen2020/-/wikis/orthology-calling)

## GitLab wiki pages

We used pre-computed blast results to run the orthology analyses presented in the manuscript.
We give details of this analysis (and eventually others) in the [GitLab wiki](https://gitlab.com/SantosRAC/afum_afma_alen2020/-/wikis/home).


## Paper abstract (Frontiers in Genetics)

Fungal pathogens are a global threat to human health. For example, fungi from the genus Aspergillus cause a spectrum of diseases collectively known as aspergillosis. Most of the >200,000 life-threatening aspergillosis infections per year worldwide are caused by Aspergillus fumigatus. Recently, molecular typing techniques have revealed that aspergillosis can also be caused by organisms that are phenotypically similar to A. fumigatus but genetically distinct, such as Aspergillus lentulus and Aspergillus fumigatiaffinis. Importantly, some of these so-called cryptic species exhibit different virulence and drug susceptibility profiles than A. fumigatus, however, our understanding of their biology and pathogenic potential has been stymied by the lack of genome sequences and phenotypic profiling. To fill this gap, we phenotypically characterized the virulence and drug susceptibility of 15 clinical strains of A. fumigatus, A. lentulus, and A. fumigatiaffinis from Spain and sequenced their genomes. We found heterogeneity in virulence and drug susceptibility across species and strains. Genes known to influence drug susceptibility (cyp51A and fks1) vary in paralog number and sequence among these species and strains and correlate with differences in drug susceptibility. Similarly, genes known to be important for virulence in A. fumigatus showed variability in number of paralogs across strains and across species. Characterization of the genomic similarities and differences of clinical strains of A. lentulus, A. fumigatiaffinis, and A. fumigatus that vary in disease-relevant traits will advance our understanding of the variance in pathogenicity between Aspergillus species and strains that are collectively responsible for the vast majority of aspergillosis infections in humans.

## Citation

[Dos Santos, Renato AC, et al](https://www.frontiersin.org/articles/10.3389/fgene.2020.00459/full). "Genomic and phenotypic heterogeneity of clinical isolates of the human pathogens Aspergillus fumigatus, Aspergillus lentulus, and Aspergillus fumigatiaffinis." Frontiers in genetics 11 (2020): 459.

## Acknowledgements

* [RokasLab, Vanderbilt University, Nashville, TN (United States of America)](https://as.vanderbilt.edu/rokaslab/) <3
* Jacob Steenwyk, Matt Mead, and Professor Antonis Rokas (bioinformatics and paper outline)
* Rafael Bastos and Lilian Pereira Silva (virulence experiments)
* Ana Alastruey-Izquierdo and Olga Riviero-Menendez (experiments with antifungals)
* Professor Gustavo H. Goldman

## Financial Support (R.A.C.S. doctorate studies)
* [São Paulo Research Foundation](http://www.fapesp.br/en/), processes # [17/21983-3 and 19/07526-4](https://bv.fapesp.br/en/pesquisador/164909/renato-augusto-correa-dos-santos/)
* Support sources of other authors/ collaborators in this work are presented in the manuscript
