install.packages("pheatmap")

library(pheatmap)
library(RColorBrewer)

setwd("determinantsVirulence")

col.pal <- c('#ffffff','#e5f5f9','#ccece6','#99d8c9','#66c2a4','#41ae76','#238b45','#006d2c','#00441b')
col.pal

VirulenceDeterminantsSpanish <- read.table("VirulenceDeterminantsCounts.txt",header=TRUE,sep="\t")

pheatmap(mat=VFs_Spanish, scale="none",
         main = "Number of Paralogs of Virulence Determinants",
         cluster_rows = TRUE, cluster_cols = TRUE, color = col.pal,
         legend=TRUE, fontsize_row=3, fontsize_col=6, breaks = c(-1,0,1,2,3,4,5,10,15,20,25,30))
