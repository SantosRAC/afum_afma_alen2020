#!/usr/bin/perl

use warnings;
use strict;

my $VFgenesFile=$ARGV[0];
my $geneSNPsFile=$ARGV[1];
my $Species=$ARGV[2];
my $polymorphismType=$ARGV[3];
my %vfAnnotation;

open(VFGENES,$VFgenesFile);

while(<VFGENES>){
 chomp;
 next if(/^geneAnnotation/);
 my($geneAnnot,$genes)=split(/\t/,$_);
 my @geneList=split(/,/,$genes);
 for my $g (@geneList){
  $vfAnnotation{$g}=$geneAnnot;
 }
}

close(VFGENES);

open(GENESNPS,$geneSNPsFile);

print "Species\tpolymorphismType\tGene\tAnnotation\tvariants_impact_HIGH\tvariants_impact_LOW\tvariants_impact_MODERATE\tvariants_impact_MODIFIER\n";

while(<GENESNPS>){
 chomp;
 next if(/^#/);
 my ($GeneName,undef,$TranscriptId,undef,$variants_impact_HIGH,$variants_impact_LOW,$variants_impact_MODERATE,$variants_impact_MODIFIER,$variants_effect_downstream_gene_variant,$variants_effect_initiator_codon_variant,$variants_effect_intron_variant,$variants_effect_missense_variant,$variants_effect_splice_acceptor_variant,$variants_effect_splice_donor_variant,$variants_effect_splice_region_variant,$variants_effect_start_lost,$variants_effect_stop_gained,$variants_effect_stop_lost,$variants_effect_stop_retained_variant,$variants_effect_synonymous_variant,$variants_effect_upstream_gene_variant)=split(/\t/,$_);
 if($vfAnnotation{$TranscriptId}){
  print "$Species\t$polymorphismType\t$TranscriptId\t$vfAnnotation{$TranscriptId}\t$variants_impact_HIGH\t$variants_impact_LOW\t$variants_impact_MODERATE\t$variants_impact_MODIFIER\n";
 }
}

close(GENESNPS);
