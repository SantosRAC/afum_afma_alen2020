#!/bin/bash
#SBATCH --mail-user=renato.a.santos@vanderbilt.edu; renatoacsantos@gmail.com
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=02-00:00:00
#SBATCH --mem=60G
#SBATCH --output=SelectVariants.out

# A. lentulus
java -jar /home/santosra/SOFTWARE/GATK-3.6/GenomeAnalysisTK.jar -T SelectVariants -R ../SNPanalysisSpanishStrainsOnly/ReferenceGenomes/CNM-CM7927.pilon.fasta -V ../SNPanalysisSpanishStrainsOnly/GenotypeGVCFs/Alen/allAlen.GenotypeGVCFs_ploidy1.vcf -selectType INDEL -o GenotypeGVCFs/Alen/allAlen.GenotypeGVCFs_ploidy1.INDELs.vcf --minIndelSize 1 --maxIndelSize 50
