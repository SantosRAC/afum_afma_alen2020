#!/media/renato/718c122e-ff56-4ee9-85ec-00601685cd39/Software/anaconda/bin/python3

# Parse the VCF from SnpEff and recovers genes of interest
import argparse
import vcf
import Bio
import sys
from collections import defaultdict

###################################
# Dependencies:
# - python 
# - biopython
# - vcf
# - sys
###################################

import sys

#Making sure you are running a version of python that works with this script.
if sys.version_info[0] != 3:
 print("This script requires Python version 3")
 sys.exit(1)

###################################
# Command line arguments
###################################

version=sys.argv[0] + ' ' + "0.1"
parser = argparse.ArgumentParser(description='Parse VCFs from SnpEff and recovers genes with high impact mutations and nonsense mutations', add_help=True)
parser.add_argument('-v','--version', action='version', version=version)
parser.add_argument('-i','--input_vcf', dest='inVCF', metavar='vcf file', type=str, help='VCF file', required=True)
parser.add_argument('-o','--out', dest='out', metavar='Information about SNPs in VCFs', type=str, help='', required=True)
parser.add_argument('-r','--reference', dest='refGenomeStrain', metavar='reference genome strain', type=str, help='Reference genome/ strain used in GATK', required=True)

# Get information from argparse (arguments)
args = parser.parse_args()
vcfInputFile = args.inVCF
refStrain = args.refGenomeStrain
outFile = args.out

annotationFilter=["feature_ablation","stop_gained","start_lost","stop_lost","splice_donor_variant","splice_acceptor_variant","rare_amino_acid_variant","structural_interaction_variant","protein_protein_contact","rearranged_at_DNA_level","bidirectional_gene_fusion","gene_fusion","frameshift_variant","inversion","duplication","exon_loss_variant","chromosome","splice_acceptor_variant&intron_variant","splice_donor_variant&intron_variant","stop_lost&splice_region_variant","stop_gained&splice_region_variant"]

outOBJ = open(outFile,"w")

outOBJ.write("Strain\tReference_Strain\tScaff_Chrom\tPositionInScaff_Chrom\tGene_ID\tref_Allele\talt_Allele\tAnnotation\tAnnotation_Impact\tFeature_Type\tFeature_ID\tchangeDNA\tchangeAA\tcDNAposLen\tAAposLen" + "\n")

vcf_python = vcf.Reader(open(vcfInputFile, 'r'))
strainList = vcf_python.samples
for record in vcf_python:
 snpPOS=record.POS
 snpCHROM=record.CHROM
 refAllele=record.REF
 for i,j in enumerate(strainList):
  strain = strainList[i]
  call=record.genotype(strain)
  generalInformation=record.INFO
  for ANNfield in generalInformation['ANN']:
   Allele,Annotation,Annotation_Impact,Gene_Name,Gene_ID,Feature_Type,Feature_ID,Transcript_BioType,Rank,HGVSc,HGVSp,cDNAposLen,CDSposLen,AAposLen,Distance,ERRORSWARNSINFO=ANNfield.split('|')
   if not (refAllele == str(call.gt_bases)):
    if not (call.gt_type == None):
     if Annotation in annotationFilter:
      outOBJ.write(strain + "\t" + refStrain + "\t" + snpCHROM + "\t" + str(snpPOS) + "\t" + Gene_ID + "\t" + refAllele + "\t" + str(call.gt_bases) + "\t" + Annotation + "\t" + Annotation_Impact + "\t" + Feature_Type + "\t" + Feature_ID + "\t" + HGVSc + "\t" + HGVSp + "\t" + cDNAposLen + "\t" + AAposLen + "\n")

outOBJ.close()
