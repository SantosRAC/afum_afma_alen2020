#!/bin/bash
#SBATCH --mail-user=renato.a.santos@vanderbilt.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=02-00:00:00
#SBATCH --mem=60G
#SBATCH --output=VariantFiltration.out

# A. lentulus
java -jar /home/santosra/SOFTWARE/GATK-3.6/GenomeAnalysisTK.jar -T VariantFiltration -R ReferenceGenomes/CNM-CM7927.pilon.fasta -V GenotypeGVCFs/Alen/allAlen.GenotypeGVCFs_ploidy1.SNPs.vcf -o GenotypeGVCFs/Alen/allAlen.GenotypeGVCFs_ploidy1.SNPs.filtered.vcf --filterExpression "QD < 2.0 || FS > 60.0 || MQ < 40.0 || SOR > 4.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0" -filterName "SNPFILTER_ALL"
