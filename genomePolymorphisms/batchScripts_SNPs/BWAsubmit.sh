#!/bin/bash
#SBATCH --mail-user=renato.a.santos@vanderbilt.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --cpus-per-task=2
#SBATCH --ntasks=1
#SBATCH --array=1-5
#SBATCH --time=01-00:00:00
#SBATCH --mem=20G
#SBATCH --output=MappingStrains_Alentulus.out

# Running BWA for strains in A. lentulus
READ1=`ls ReadsAlentulus/*1P* | head -n $SLURM_ARRAY_TASK_ID | tail -n1`
READ2=${READ1/_Filtered_1P.fq.gz/_Filtered_2P.fq.gz};
STRAIN=`echo $READ1 | sed 's/ReadsAlentulus\///g' | sed 's/_Filtered_1P.fq.gz//g'`
GENOMEINDEX=ReferenceGenomes/CNM-CM7927.pilon.fasta
/home/santosra/SOFTWARE/bwa-0.7.17/bwa mem -R "@RG\tID:group1\tSM:$STRAIN\tPL:illumina\tLB:lib1\tPU:unit1" -M -t $SLURM_CPUS_PER_TASK $GENOMEINDEX $READ1 $READ2 | samtools view -@ $SLURM_CPUS_PER_TASK -b -O BAM - | samtools sort -@ $SLURM_CPUS_PER_TASK -o MappingReads/Alen/"$STRAIN".sorted.bam -O BAM -
