#!/bin/bash
#SBATCH --mail-user=renato.a.santos@vanderbilt.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --cpus-per-task=2
#SBATCH --ntasks=1
#SBATCH --array=1-9
#SBATCH --time=24:00:00
#SBATCH --mem=30G
#SBATCH --output=MarkDuplicatesAfumigatusComplex_%A_%a.out

# Running Picard to mark duplicates in all BAM files
INPUTBAM=`ls MappingReads/*/*.sorted.bam | head -n $SLURM_ARRAY_TASK_ID| tail -n1`
METRICSFILE=${INPUTBAM/.sorted.bam/_markdup_metrics.txt}
OUTBAM=${INPUTBAM/.sorted.bam/.sorted_markdup.bam}
java -Xmx30g -jar /home/santosra/SOFTWARE/picard/picard.jar MarkDuplicates I="$INPUTBAM" O="$OUTBAM" METRICS_FILE="$METRICSFILE"
