#!/bin/bash
#SBATCH --mail-user=renato.a.santos@vanderbilt.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --cpus-per-task=2
#SBATCH --ntasks=1
#SBATCH --array=1-9
#SBATCH --time=24:00:00
#SBATCH --mem=20G
#SBATCH --output=SAMTOOLS_IndexBAMsPE_%A_%a.out

# Running SAMTOLS indexing
INPUTBAM=`ls MappingReads/A*/*sorted_markdup.bam | head -n $SLURM_ARRAY_TASK_ID | tail -n1`
samtools index -@ $SLURM_CPUS_PER_TASK $INPUTBAM
