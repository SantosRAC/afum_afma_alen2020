#!/bin/bash
#SBATCH --mail-user=renato.a.santos@vanderbilt.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --cpus-per-task=2
#SBATCH --ntasks=1
#SBATCH --array=1-5
#SBATCH --time=24:00:00
#SBATCH --mem=40G
#SBATCH --output=GATK_RealignerTCreator_Alentulus_%A_%a_.out

# Running GATK RealignerTargetCreator
INPUTBAM=`ls MappingReads/Alen/*_markdup.bam | head -n $SLURM_ARRAY_TASK_ID| tail -n1`
STRAIN=`echo $INPUTBAM | cut -f3 -d'/'`
STRAIN=${STRAIN%.sorted_markdup.bam}
REFGENOME=ReferenceGenomes/CNM-CM7927.pilon.fasta
java -Xmx40g -jar /home/santosra/SOFTWARE/GATK-3.6/GenomeAnalysisTK.jar -T RealignerTargetCreator -I $INPUTBAM -R "$REFGENOME" -o MappingReads/Alen/"$STRAIN".list
