#!/bin/bash
#SBATCH --mail-user=renato.a.santos@vanderbilt.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --cpus-per-task=2
#SBATCH --ntasks=1
#SBATCH --array=1-5
#SBATCH --time=24:00:00
#SBATCH --mem=60G
#SBATCH --output=GATK_IndelRealigner_Alentulus_%A_%a_.out

# Running GATK IndelRealigner
INPUTBAM=`ls MappingReads/Alen/*.sorted_markdup.bam | head -n $SLURM_ARRAY_TASK_ID| tail -n1`
STRAIN=`echo $INPUTBAM | cut -f3 -d'/'`
STRAIN=${STRAIN%.sorted_markdup.bam}
REFGENOME=ReferenceGenomes/CNM-CM7927.pilon.fasta
OUTBAM=${INPUTBAM/sorted_markdup.bam/sorted_markdup_cleaned.bam}
java -Xmx60g -jar /home/santosra/SOFTWARE/GATK-3.6/GenomeAnalysisTK.jar -T IndelRealigner -I $INPUTBAM -R $REFGENOME -targetIntervals MappingReads/Alen/"$STRAIN".list -o $OUTBAM
