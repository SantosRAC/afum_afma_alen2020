#!/bin/bash
#SBATCH --mail-user=renato.a.santos@vanderbilt.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=02-00:00:00
#SBATCH --mem=60G
#SBATCH --output=GenotypeGVCFs.out

# A. lentulus
SPECIES="Alen"
GENOME=ReferenceGenomes/CNM-CM7927.pilon.fasta
java -Xmx60g -jar /home/santosra/SOFTWARE/GATK-3.6/GenomeAnalysisTK.jar -T GenotypeGVCFs -R $GENOME --max_alternate_alleles 10 -o GenotypeGVCFs/"$SPECIES"/all"$SPECIES".GenotypeGVCFs_ploidy1.vcf --variant HaplotypeCaller/Alen/hcaller_CNM-CM6069_ploidy1_initialCalling.g.vcf --variant HaplotypeCaller/Alen/hcaller_CNM-CM6936_ploidy1_initialCalling.g.vcf --variant HaplotypeCaller/Alen/hcaller_CNM-CM8060_ploidy1_initialCalling.g.vcf --variant HaplotypeCaller/Alen/hcaller_CNM-CM8694_ploidy1_initialCalling.g.vcf --variant HaplotypeCaller/Alen/hcaller_CNM-CM8927_ploidy1_initialCalling.g.vcf
