#!/media/renato/718c122e-ff56-4ee9-85ec-00601685cd39/Software/anaconda/bin/python3

from Bio import SeqIO
import argparse

parser = argparse.ArgumentParser(description="Split the fasta file into individual file with each gene seq")
parser.add_argument('-f', action='store', dest='fasta_file', help='Input fasta file', required=True)
result = parser.parse_args()

numSeqs = len([1 for line in open(result.fasta_file) if line.startswith(">")])
print ("Number of sequences in FASTA is ", numSeqs)

f_open = open(result.fasta_file, "rU")

countOutputSeqs=1
fileNameNum=1000

for rec in SeqIO.parse(f_open, "fasta"):
   id = rec.id
   seq = rec.seq
   if countOutputSeqs > 1000:
    countOutputSeqs=1
    fileNameNum+=1000
   fileName=str(fileNameNum)+".fasta"
   id_file = open(fileName, "a+")
   id_file.write(">"+str(id)+"\n"+str(seq)+"\n")
   id_file.close()
   countOutputSeqs+=1

f_open.close()
