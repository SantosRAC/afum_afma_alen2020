#!/usr/bin/perl

use warnings;
use strict;

#TODO: Still need to use Getopt::Long to parse command-line arguments

my $speciesIDs=$ARGV[0];
my $sequenceIDs=$ARGV[1];
my $blastResults=$ARGV[2];

my %SpeciesID2SpeciesIndex;
my %SequeceID2SequenceIndex;

open(SPECIESIDF,$speciesIDs);

while(<SPECIESIDF>){
 chomp;
 my ($speciesIndex,$speciesName)=split(/\s/,$_);
 $speciesIndex=~s/://g;
 $speciesName=~s/.orig.fasta//g;
 #print "$speciesIndex\n";
 #print "$speciesName\n";
 $SpeciesID2SpeciesIndex{$speciesName}=$speciesIndex;
}

close(SPECIESIDF);

open(SEQUENCEIDSF,$sequenceIDs);

while(<SEQUENCEIDSF>){
 chomp;
 my ($sequenceIndex,$sequenceName)=split(/\s/,$_);
 $sequenceIndex=~s/://g;
 $SequeceID2SequenceIndex{$sequenceName}=$sequenceIndex;
 #print "$sequenceIndex\n";
 #print "$sequenceName\n";
}

close(SEQUENCEIDSF);

open(BLASTRESF,$blastResults);

while(<BLASTRESF>){
 chomp;
 my @fields=split(/\t/,$_);
 my $sps1field=$fields[0];
 my $sps2field=$fields[1];
 if (!$SequeceID2SequenceIndex{$sps1field}){
  die "Something wrong...\n";
 }
 if (!$SequeceID2SequenceIndex{$sps2field}){
  print "$sps2field\n";
  die "Something wrong...\n";
 }
 my @sps1fields=split(/\|/,$sps1field);
 my @sps2fields=split(/\|/,$sps2field);
 my $sps1=$sps1fields[0];
 my $sps2=$sps2fields[0];
 $fields[0]=$SequeceID2SequenceIndex{$sps1field};
 $fields[1]=$SequeceID2SequenceIndex{$sps2field};
 my $outfile="Blast".$SpeciesID2SpeciesIndex{$sps1}."_".$SpeciesID2SpeciesIndex{$sps2}.".txt";
 print "Printing to $outfile\n";
 open(OUTFILE,'>>',$outfile);
 print OUTFILE join("\t",@fields)."\n";
 close(OUTFILE);
}

close(BLASTRESF);
