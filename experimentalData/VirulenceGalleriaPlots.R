library(ggplot2)
library(gridExtra)

setwd("/media/renato/718c122e-ff56-4ee9-85ec-00601685cd39/Repositories/paper1_rokaslab_renatosantosphd/ExperimentalResults")

Survivals <- read.table("VirulenceCurve2.txt",head=TRUE, sep="\t")

Survivals

SurvivalsAfum <- rbind(Survivals[Survivals$Species == "Aspergillus fumigatus",], Survivals[Survivals$Species == "PBS",])
SurvivalsAlen <- rbind(Survivals[Survivals$Species == "Aspergillus lentulus",], Survivals[Survivals$Species == "PBS",])
SurvivalsAfumigatiaffinis <- rbind(Survivals[Survivals$Species == "Aspergillus fumigatiaffinis",], Survivals[Survivals$Species == "PBS",])

p1 <- ggplot(data=SurvivalsAfum, aes(x=Day, y=Survival, fill=Strain, color=Strain)) +
  geom_line(aes(color=Strain)) +
  geom_point() +
  theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
  labs(title="A. fumigatus")

p2 <- ggplot(data=SurvivalsAlen, aes(x=Day, y=Survival, fill=Strain, color=Strain)) +
  geom_line(aes(color=Strain)) +
  geom_point() +
  theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
  labs(title="A. lentulus")

p3 <- ggplot(data=SurvivalsAfumigatiaffinis, aes(x=Day, y=Survival, fill=Strain, color=Strain)) +
  geom_line(aes(color=Strain)) +
  geom_point() +
  theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
  labs(title="A. fumigatiaffinis")

grid.arrange(p1, p2, p3, nrow = 3, top = "Cumulative survival of Galleria mellonella (Virulence)")