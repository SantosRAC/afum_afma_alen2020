#!/media/renato/718c122e-ff56-4ee9-85ec-00601685cd39/Software/anaconda/bin/python3.6

from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio import SeqIO
import argparse
import sys

version=sys.argv[0] + ' ' + "0.1"
parser = argparse.ArgumentParser(description='Extracts promoter regions', add_help=True)
parser.add_argument('-v','--version', action='version', version=version)
parser.add_argument('-i','--input_fasta', dest='inFASTA', metavar='FASTA file', type=str, help='FASTA file', required=True)
parser.add_argument('--scaffold', dest='inSCAFF', metavar='Scaffold name', type=str, help='Scaffold name', required=True)
parser.add_argument('--strain', dest='inSTRAIN', metavar='Strain identifier', type=str, help='Strain identifier', required=True)
parser.add_argument('-s','--strand', dest='seqStrand', metavar='sequence strand', type=str, help='Sequence strand', required=True)
parser.add_argument('--start', dest='geneStart', metavar='Gene start position', type=str, help='Gene start position', required=True)

args = parser.parse_args()
sequenceFasta = args.inFASTA
geneScaffold = args.inSCAFF
dnaStrand = args.seqStrand
geneStartPosition = args.geneStart
strainID = args.inSTRAIN

# CNM-CM6069	NODE_14_length_282742_cov_37.546051_pilon	90442	92060	+	NODE_14_length_282742_cov_37.5.g1673
if dnaStrand == "plus":
 for seq_record in SeqIO.parse(sequenceFasta,"fasta"):
  if seq_record.id == geneScaffold:
   promoterStartIndex=int(geneStartPosition)-401
   promoterEndIndex=promoterStartIndex+409
   #How it would be to extract first 9 bases in coding sequence in plus strand
   #promoterStartIndex=int(geneStartPosition)-1
   #promoterEndIndex=promoterStartIndex+10
   print(">"+strainID)
   print(seq_record.seq[promoterStartIndex:promoterEndIndex])
elif dnaStrand == "minus":
 for seq_record in SeqIO.parse(sequenceFasta,"fasta"):
  if seq_record.id == geneScaffold:
   promoterEndIndex=int(geneStartPosition)+400
   promoterStartIndex=promoterEndIndex-409
   #How it would be to extract first 9 bases in coding sequence in minus strand
   #promoterStartIndex=int(geneStartPosition)-9
   #promoterEndIndex=promoterStartIndex+9
   seq_record2=seq_record.seq[promoterStartIndex:promoterEndIndex]
   seq_record3=seq_record2.reverse_complement()
   print(">"+strainID)
   print(seq_record3)
else:
 print("Something is really wrong with your code!")
 sys.exit(1)
