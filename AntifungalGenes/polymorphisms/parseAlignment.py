#!/media/renato/718c122e-ff56-4ee9-85ec-00601685cd39/Software/anaconda/bin/python3

import argparse
from Bio import SeqIO
import sys
import collections

minPosOBJ=0
maxPosOBJ=100000000000000

version=sys.argv[0] + ' ' + "0.1"
parser = argparse.ArgumentParser(description='Parses FASTA alignments', add_help=True)
parser.add_argument('-v','--version', action='version', version=version)
parser.add_argument('-i','--input_fasta', dest='inFASTA', metavar='FASTA file', type=str, help='FASTA file', required=True)
parser.add_argument('-r','--reference', dest='refStrain', metavar='reference strain', type=str, help='Reference strain', required=True)
parser.add_argument('--min_pos', dest='minPos', metavar='min position ref', type=str, help='Min position in reference sequence', required=False)
parser.add_argument('--max_pos', dest='maxPos', metavar='max position ref', type=str, help='Max position in reference sequence', required=False)

def makehash():
 return collections.defaultdict(makehash)

args = parser.parse_args()
fastaAlignmentFile = args.inFASTA
refStrainOBJ = args.refStrain
if args.minPos:
 minPosOBJ = args.minPos
if args.maxPos:
 maxPosOBJ = args.maxPos

refBasePosition = makehash()
otherBasePosition = makehash()
positionAAAlignment = makehash()

print("Strain\tStrain_Seq_Base\talignment_Pos\trefBasePosition\tStrain_BasePosition\tAA_Ref")

for seq_record in SeqIO.parse(fastaAlignmentFile, "fasta"):
 positionAlignment=1
 if seq_record.id == refStrainOBJ:
  positionProteinRef=1
  for seqBase in seq_record.seq:
   if seqBase == "-":
    refBasePosition[positionAlignment]=positionProteinRef
    positionAAAlignment[positionAlignment][seq_record.id]=seqBase
    positionAlignment+=1
   else:
    refBasePosition[positionAlignment]=positionProteinRef
    positionAAAlignment[positionAlignment][seq_record.id]=seqBase
    positionProteinRef+=1;
    positionAlignment+=1

for seq_record in SeqIO.parse(fastaAlignmentFile, "fasta"):
 positionAlignment=1
 if seq_record.id != refStrainOBJ:
  positionProteinOther=1
  for seqBase in seq_record.seq:
   if seqBase == "-":
    otherBasePosition[seq_record.id][positionAlignment]=positionProteinOther
    positionAAAlignment[positionAlignment][seq_record.id]=seqBase
    if (seqBase != positionAAAlignment[positionAlignment][refStrainOBJ]) and (refBasePosition[positionAlignment] >= int(minPosOBJ)) and (refBasePosition[positionAlignment] <= int(maxPosOBJ)):
     print(seq_record.id,"\t",seqBase,"\t",positionAlignment,"\t",refBasePosition[positionAlignment],"\t",otherBasePosition[seq_record.id][positionAlignment],"\t",positionAAAlignment[positionAlignment][refStrainOBJ]) 
    positionAlignment+=1
   else:
    otherBasePosition[seq_record.id][positionAlignment]=positionProteinOther
    positionAAAlignment[positionAlignment][seq_record.id]=seqBase
    if (seqBase != positionAAAlignment[positionAlignment][refStrainOBJ]) and (refBasePosition[positionAlignment] >= int(minPosOBJ)) and (refBasePosition[positionAlignment] <= int(maxPosOBJ)):
     print(seq_record.id,"\t",seqBase,"\t",positionAlignment,"\t",refBasePosition[positionAlignment],"\t",otherBasePosition[seq_record.id][positionAlignment],"\t",positionAAAlignment[positionAlignment][refStrainOBJ]) 
    positionAlignment+=1
    positionProteinOther+=1
