#!/usr/bin/perl

use warnings;
use strict;

my $InterProScan=$ARGV[0];
my %gene2goTerms;

open(INTERPROSCANF,$InterProScan);

while(<INTERPROSCANF>){
 chomp;
 my($gene,undef,undef,undef,undef,undef,undef,undef,undef,undef,undef,undef,undef,$GOterms)=split(/\t/,$_);
 if($GOterms){
  my @gotermsArray=split(/\|/,$GOterms);
  for my $e (@gotermsArray){
   if($gene2goTerms{$gene}){
    unless($e ~~ @{$gene2goTerms{$gene}}){
     push(@{$gene2goTerms{$gene}},$e);
    }
   }else{
    @{$gene2goTerms{$gene}}=($e);
   }
  }
 }
}

close(INTERPROSCANF);

for my $key (keys %gene2goTerms){
 print "$key\t".join(";",@{$gene2goTerms{$key}})."\n";
}
